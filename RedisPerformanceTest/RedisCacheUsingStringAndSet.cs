﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedisPerformanceTest
{
    public class RedisCacheUsingStringAndSet<T> : IRedisCache<T>
    {
        private readonly IDatabase cache;
        private string setKey;

        public RedisCacheUsingStringAndSet(IDatabase cache)
        {
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
            setKey = "setKey";
        }

        public Task<T> AddOrUpdate(string key, T item)
        {
            return AddOrUpdateItem(key, item);
        }

        private async Task<T> AddOrUpdateItem(string key, T item)
        {
            await cache.StringSetAsync(key, Serialize(item));
            await cache.SetAddAsync(setKey, key);

            return item;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            var items = new List<T>();
            var itemCacheKeys = await cache.SetMembersAsync(setKey);
            foreach (var cacheKey in itemCacheKeys)
            {
                var maybeValue = await cache.StringGetAsync((string)cacheKey);
                if (maybeValue.HasValue)
                {
                    items.Add(Deserialize(maybeValue));
                }
                else
                {
                    await cache.SetRemoveAsync(setKey, cacheKey);
                }
            }

            return items;
        }

        public Task Remove(string key)
        {
            throw new NotImplementedException();
        }

        private string Serialize(T item)
        {
            return JsonConvert.SerializeObject(item);
        }

        private T Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
