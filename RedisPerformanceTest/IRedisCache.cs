﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedisPerformanceTest
{
    public interface IRedisCache<T>
    {
        Task<T> AddOrUpdate(string key, T item);
        Task<IEnumerable<T>> GetAll();
        Task Remove(string key);
    }
}