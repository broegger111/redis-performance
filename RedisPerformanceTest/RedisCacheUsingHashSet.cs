﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedisPerformanceTest
{
    public class RedisCacheUsingHashSet<T> : IRedisCache<T>
    {
        private readonly IDatabase cache;
        private string hashKey;
        public RedisCacheUsingHashSet(IDatabase cache)
        {
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
            hashKey = typeof(T).Name;
        }
        public async Task<T> AddOrUpdate(string key, T item)
        {
            await cache.HashSetAsync(hashKey, key, Serialize(item));
            return item;
        }

        public async Task Remove(string key)
        {
            await cache.HashDeleteAsync(hashKey, key);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            var all = await cache.HashGetAllAsync(hashKey);
            return all.Select(x => Deserialize(x.Value));
        }

        private string Serialize(T item)
        {
            return JsonConvert.SerializeObject(item);
        }

        private T Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
