﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedisPerformanceTest
{
    public class TypeToCache
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Secret { get; set; }
    }
}
