﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RedisPerformanceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Redis!!!");
            AsyncMain().Wait();
            Console.ReadKey();
        }

        static async Task AsyncMain()
        {
            Stopwatch stopWatch = new Stopwatch();
            var redis = ConnectionMultiplexer.Connect("localhost");

            var itemsToCache = new List<TypeToCache>();
            for (int i = 0; i < 200; i++)
            {
                itemsToCache.Add(Generate());
            }
            Console.WriteLine("Press S to use stringSet. Press any for hashSet");
            IRedisCache<TypeToCache> cache = new RedisCacheUsingHashSet<TypeToCache>(redis.GetDatabase());
            var key = Console.ReadKey();
            if (key.Key == ConsoleKey.S)
            {
                cache = new RedisCacheUsingStringAndSet<TypeToCache>(redis.GetDatabase());
            }
            
            Console.WriteLine("Adding items to cache");
            foreach(var item in itemsToCache)
            {
                await cache.AddOrUpdate(item.Id, item);
            }
            Console.WriteLine("Press any key to start reading");
            Console.ReadKey();
            stopWatch.Start();
            var itemsFromCache = await cache.GetAll();
            stopWatch.Stop();
            Console.WriteLine($"Read {itemsFromCache.Count()} items. Elapsed time: {stopWatch.ElapsedMilliseconds} ms");
            
        }

        public static TypeToCache Generate()
        {
            var id = Guid.NewGuid().ToString();
            return new TypeToCache
            {
                Id = id,
                Name = $"Name:{id}",
                Address = $"Address:{id}",
                Secret = $"Secret:{id}"
            };
        }
    }
}
